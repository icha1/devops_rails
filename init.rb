#!/usr/bin/env ruby
# frozen_string_literal: true

environment = ENV['RAILS_ENV']

container = ARGV[0] || 'api'

case container
when 'api'
  %w[db:create db:migrate].each do |rake_name|
    puts "-> rake #{rake_name}"
    exec "bundle exec rails #{rake_name}" unless system "bundle exec rails #{rake_name}"
  end
  exec "bundle exec puma -e #{environment}"
end
