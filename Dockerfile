FROM ruby:2.6.5-alpine3.9 as builder

WORKDIR /app

ARG RAILS_MASTER_KEY

ENV BUNDLE_SILENCE_ROOT_WARNING=1 \
    BUNDLE_IGNORE_MESSAGES=1 \
    BUNDLE_GITHUB_HTTPS=1 \
    BUNDLE_FROZEN=1 \
    BUNDLE_WITHOUT=development:test \
    DATABASE_URL=postgres://myuser:mypass@localhost/somedatabase

RUN apk update && apk add --no-cache \
        build-base postgresql-dev libxml2-dev libxslt-dev nodejs yarn tzdata

COPY Gemfile* ./
RUN gem install bundler && bundle install -j4 --retry 3

# Add the Rails app
COPY . /app        
RUN mv ./config/database.yml.sample ./config/database.yml && \
        bundle exec rake assets:precompile RAILS_ENV=production && \
        # Clean up files (cached *.gem, *.o, *.c)
        rm -rf /usr/local/bundle/cache/*.gem && \
        find /usr/local/bundle/gems/ -name "*.c" -delete && \
        find /usr/local/bundle/gems/ -name "*.o" -delete            

FROM ruby:2.6.5-alpine3.9

ARG RAILS_ENV=production

WORKDIR /app
COPY --from=builder /usr/local/bundle /usr/local/bundle
COPY --from=builder /app ./

RUN apk update && apk add --no-cache \
        postgresql-client tzdata nodejs && \
        addgroup -g 1000 -S appuser && \
        adduser -u 1000 -S appuser -G appuser && \
        chown appuser:appuser /app -R 

EXPOSE 3000
USER appuser

CMD ["ruby", "init.rb"]